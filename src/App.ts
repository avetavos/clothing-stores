import express, { Application } from 'express';
import loader from './app/loaders';
import Logger from './app/loaders/logger';
import dotenv from 'dotenv';

dotenv.config();

async function startServer() {
  const app: Application = express();

  await loader({ expressApp: app });

  app.listen(process.env.PORT, err => {
    if (err) {
      Logger.error(err);
      process.exit(1);
      return;
    }
    Logger.info(`🛡️ Server listening on port: ${process.env.PORT}`);
  });
}

startServer();
