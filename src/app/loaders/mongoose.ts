import mongoose from 'mongoose';
import { Db } from 'mongodb';

export default async (): Promise<Db> => {
  const { DB_USER, DB_PASSWORD, DB_PATH } = process.env;
  const connection = await mongoose.connect(
    `mongodb://${DB_USER}:${DB_PASSWORD}${DB_PATH}`,
    {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,
      useFindAndModify: false
    }
  );
  return connection.connection.db;
};
