import { Application } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';
import compression from 'compression';

import AuthenticationController from '../controller/authentication';
import ProductController from '../controller/product';
import ReceiptController from '../controller/receipt';

const controllers = [
  new AuthenticationController(),
  new ProductController(),
  new ReceiptController()
];

export default ({ app }: { app: Application }) => {
  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(compression());
  if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
  }
  controllers.forEach(controller => {
    app.use('/api', controller.router);
  });
};
