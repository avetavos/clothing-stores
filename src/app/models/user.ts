import { Schema, Document, model } from 'mongoose';
import IUser from '../interfaces/user';

const userSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    username: {
      type: String,
      unique: true,
      index: true
    },
    password: String,
    role: {
      type: String,
      default: 'user'
    },
    phone: String,
    facebook: String,
    line: String,
    address: String
  },
  { timestamps: true }
);

const userModel = model<Document & IUser>('user', userSchema);

export default userModel;
