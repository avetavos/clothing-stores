import { Schema, Document, model } from 'mongoose';
import IReceipt from '../interfaces/receipt';

const receiptSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    },
    list: [
      {
        _id: {
          type: Schema.Types.ObjectId,
          ref: 'product'
        },
        name: {
          type: String,
          required: true
        },
        quantity: Number,
        price: Number,
        amount: Number
      }
    ],
    receipt: String,
    description: String,
    slip: String
  },
  { timestamps: true }
);

const receiptModel = model<Document & IReceipt>('receipt', receiptSchema);

export default receiptModel;
