import { Schema, Document, model } from 'mongoose';
import IProduct from '../interfaces/product';

const productSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  barcode: {
    type: String,
    required: true,
    unique: true
  },
  price: {
    type: Number,
    required: true
  },
  cost: {
    type: Number,
    required: true
  },
  quantity: {
    type: Number,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

const productModel = model<Document & IProduct>('product', productSchema);

export default productModel;
