import { check } from 'express-validator';

const validator = [
  check('name', 'Name is required')
    .not()
    .isEmpty()
];

export default validator;
