import { check } from 'express-validator';

const validator = [
  check('name', 'Name is required')
    .not()
    .isEmpty(),
  check('username', 'Username is required')
    .not()
    .isEmpty(),
  check(
    'password',
    'Please enter a password with 6 or more characters'
  ).isLength({ min: 6 })
];

export default validator;
