import { Response, NextFunction } from 'express';
import RequestWithUser from '../../interfaces/requestWithUser';
import jwt from 'jsonwebtoken';

export const AccessibleUserMiddleware = async (
  req: RequestWithUser,
  res: Response,
  next: NextFunction
) => {
  const token = req.header('x-auth-token');
  try {
    const decoded: any = await jwt.verify(token, process.env.SECRET_KEY);
    const user = decoded.user;
    if (user.role === 'user') {
      return next();
    }
    return res.status(401).json({ errors: [{ msg: 'Inaccessible' }] });
  } catch (err) {
    return res.status(401).json({ errors: [{ msg: 'Token is not valid' }] });
  }
};

export const AccessibleOwnerMiddleware = async (
  req: RequestWithUser,
  res: Response,
  next: NextFunction
) => {
  const token = req.header('x-auth-token');
  try {
    const decoded: any = await jwt.verify(token, process.env.SECRET_KEY);
    const user = decoded.user;
    if (user.role === 'owner') {
      return next();
    }
    return res.status(401).json({ errors: [{ msg: 'Inaccessible' }] });
  } catch (err) {
    return res.status(401).json({ errors: [{ msg: 'Token is not valid' }] });
  }
};
