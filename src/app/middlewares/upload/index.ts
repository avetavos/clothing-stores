import multer from 'multer';

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    if (process.env.NODE_ENV === 'development') {
      cb(null, 'src/app/storages/slip_images');
    } else {
      cb(null, 'dist/app/storages/slip_images');
    }
  },
  filename: (req, file, cb) => {
    var filetype = '';
    if (file.mimetype === 'image/png') {
      filetype = 'png';
    }
    if (file.mimetype === 'image/jpeg') {
      filetype = 'jpg';
    }
    cb(null, 'slip-image-' + Date.now() + '.' + filetype);
  }
});

export default multer({ storage: storage });
