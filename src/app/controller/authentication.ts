import { Router, Request, Response } from 'express';
import { validationResult } from 'express-validator';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';

import UserModel from '../models/user';
import AuthMiddleware from '../middlewares/authentication';
import { AccessibleOwnerMiddleware } from '../middlewares/accession';
import LoginValidation from '../middlewares/validations/login';
import RequestWithUser from '../interfaces/requestWithUser';
import Logger from '../loaders/logger';

export default class AuthenticationController {
  private path = '/auth';
  public router = Router();
  private User = UserModel;

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}/register`, this.register);
    this.router.post(`${this.path}/login`, LoginValidation, this.login);
    this.router.get(`${this.path}/current`, AuthMiddleware, this.currentUser);
    this.router.put(
      `${this.path}/:_id`,
      [AuthMiddleware, AccessibleOwnerMiddleware],
      this.updateUser
    );
    this.router.delete(
      `${this.path}/:_id`,
      [AuthMiddleware, AccessibleOwnerMiddleware],
      this.deleteUser
    );
    this.router.get(`${this.path}/all`, AuthMiddleware, this.allUser);
    this.router.get(
      `${this.path}/:_id`,
      [AuthMiddleware, AccessibleOwnerMiddleware],
      this.getUser
    );
  }

  private register = async (req: RequestWithUser, res: Response) => {
    const { name, username, password, role } = req.body;
    try {
      let user = await this.User.findOne({ username });
      if (user) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'User already exists' }] });
      }
      user = new this.User({
        name,
        username,
        password,
        role
      });
      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(password, salt);
      await user.save();
      user.password = await undefined;
      return res.status(201).json(user);
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };

  private login = async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { username, password } = req.body;
    try {
      const user = await this.User.findOne({ username });
      if (!user) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Invalid credentials' }] });
      }
      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Invalid credentials' }] });
      }
      const payload = {
        user: {
          _id: user._id,
          name: user.name,
          role: user.role
        }
      };
      jwt.sign(payload, process.env.SECRET_KEY, (err, token) => {
        res.json({
          success: true,
          token
        });
      });
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };

  private currentUser = async (req: RequestWithUser, res: Response) => {
    await this.User.findById({ _id: req.user._id })
      .select('-password')
      .then(user => {
        return res.status(200).json(user);
      });
  };

  private updateUser = async (req: RequestWithUser, res: Response) => {
    const { name, password, role, phone, line, facebook, address } = req.body;
    const salt = await bcrypt.genSalt(10);
    const userFields: any = {};
    if (name) userFields.name = name;
    if (password) userFields.password = await bcrypt.hash(password, salt);
    if (role) userFields.role = role;
    if (phone) userFields.phone = phone;
    if (line) userFields.line = line;
    if (facebook) userFields.facebook = facebook;
    if (address) userFields.address = address;
    try {
      const user = await this.User.findOneAndUpdate(
        { _id: req.params._id },
        { $set: userFields },
        { new: true, upsert: true }
      );
      user.password = await undefined;
      return res.status(200).json(user);
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };

  private deleteUser = async (req: RequestWithUser, res: Response) => {
    try {
      await this.User.findByIdAndDelete(req.params._id);
      return res.status(200).json('User is deleted');
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };

  private allUser = async (req: RequestWithUser, res: Response) => {
    try {
      const users = await this.User.find().select('-password');
      if (!users) {
        return res.status(404).json({ errors: [{ msg: 'User not found' }] });
      }
      return res.status(200).json(users);
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };

  private getUser = async (req: RequestWithUser, res: Response) => {
    try {
      const user = await this.User.findById(req.params._id).select('-password');
      if (!user) {
        return res.status(404).json({ errors: [{ msg: 'User not found' }] });
      }
      return res.status(200).json(user);
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };
}
