import { Router, Response } from 'express';

import Logger from '../loaders/logger';
import ProductModel from '../models/product';
import AuthMiddleware from '../middlewares/authentication';
import { AccessibleOwnerMiddleware } from '../middlewares/accession';
import RequestWithUser from '../interfaces/requestWithUser';

export default class ProductController {
  private path = '/products';
  public router = Router();
  private Product = ProductModel;

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(this.path, AuthMiddleware, this.getAllProducts);
    this.router.get(`${this.path}/:_id`, AuthMiddleware, this.getProductById);
    this.router.post(
      this.path,
      [AuthMiddleware, AccessibleOwnerMiddleware],
      this.createProduct
    );
    this.router.put(
      `${this.path}/:_id`,
      [AuthMiddleware, AccessibleOwnerMiddleware],
      this.updateProduct
    );
    this.router.delete(
      `${this.path}/:_id`,
      [AuthMiddleware, AccessibleOwnerMiddleware],
      this.deleteProduct
    );
    this.router.get(
      `${this.path}/barcode/:barcode/:quantity`,
      AuthMiddleware,
      this.findItemByBarcode
    );
  }

  private getProductById = async (req: RequestWithUser, res: Response) => {
    try {
      const product = await this.Product.findById(req.params._id);
      if (!product) {
        return res.status(404).json({
          errors: [{ msg: `Product id ${req.params._id} not found` }]
        });
      }
      return res.status(200).json(product);
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };

  private getAllProducts = async (req: RequestWithUser, res: Response) => {
    try {
      const products = await this.Product.find().sort('name');
      if (!products) {
        return res.status(404).json({ errors: [{ msg: 'Product not found' }] });
      }
      return res.status(200).json(products);
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };

  private createProduct = async (req: RequestWithUser, res: Response) => {
    const { name, barcode, price, cost, quantity } = req.body;
    const productField: any = {};
    if (name) productField.name = name;
    if (barcode) productField.barcode = barcode;
    if (price) productField.price = price;
    if (cost) productField.cost = cost;
    if (quantity) productField.quantity = quantity;
    try {
      let product = await new this.Product(productField);
      await product.save();
      return res.status(200).json(product);
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };

  private updateProduct = async (req: RequestWithUser, res: Response) => {
    const { name, barcode, price, cost, quantity } = req.body;
    const productField: any = {};
    if (name) productField.name = name;
    if (barcode) productField.barcode = barcode;
    if (price) productField.price = price;
    if (cost) productField.cost = cost;
    if (quantity) productField.quantity = quantity;
    try {
      let product = await this.Product.findOneAndUpdate(
        { _id: req.params._id },
        { $set: productField },
        { new: true }
      );
      return res.status(200).json(product);
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };

  private deleteProduct = async (req: RequestWithUser, res: Response) => {
    try {
      await this.Product.findByIdAndDelete(req.params._id);
      return res.status(200).json('Product is deleted');
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };

  private findItemByBarcode = async (req: RequestWithUser, res: Response) => {
    try {
      const { barcode, quantity } = req.params;
      const product = await this.Product.findOne({
        barcode,
        quantity: { $gte: quantity }
      });
      let showProduct = await {
        _id: product._id,
        name: product.name,
        barcode: product.barcode,
        price: product.price,
        cost: product.cost,
        quantity,
        store: product.quantity
      };
      return res.status(200).json(showProduct);
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };
}
