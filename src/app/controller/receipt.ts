import { Router, Response, Request } from 'express';
import path from 'path';

import Logger from '../loaders/logger';
import ReceiptModel from '../models/receipt';
import AuthMiddleware from '../middlewares/authentication/';
import RequestWithUser from '../interfaces/requestWithUser';
import RequestWithUserAndFile from '../interfaces/requestWithUserAndFile';
import Upload from '../middlewares/upload';

export default class ReceiptController {
  public router = Router();
  private path = '/receipts';
  private Receipt = ReceiptModel;

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(
      this.path,
      AuthMiddleware,
      Upload.array('photo'),
      this.createReceipt
    );
    this.router.get(this.path, AuthMiddleware, this.getAllReceipt);
    this.router.get(`${this.path}/:_id`, AuthMiddleware, this.getReceiptById);
    this.router.delete(`${this.path}/:_id`, AuthMiddleware, this.deleteReceipt);
    this.router.get(`${this.path}/slip/:filename`, this.getSlipImage);
  }

  private createReceipt = async (
    req: RequestWithUserAndFile,
    res: Response
  ) => {
    const slip = req.files[0].filename;
    const description = req.body.description;
    const list = JSON.parse(req.body.list);
    list.map(item => {
      item.amount = item.price * item.quantity;
    });
    const receiptField = {
      user: req.user._id,
      list,
      description,
      slip
    };
    try {
      let receipt = await new this.Receipt(receiptField);
      await receipt.save();
      return res.status(201).json(receipt);
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };

  private getAllReceipt = async (req: RequestWithUser, res: Response) => {
    try {
      const receipts = await this.Receipt.find()
        .sort({ createdAt: 'desc' })
        .populate('user');
      return res.status(200).json(receipts);
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };

  private getReceiptById = async (req: RequestWithUser, res: Response) => {
    try {
      const receipt = await this.Receipt.findById(req.params._id).sort({
        date: -1
      });
      if (!receipt) {
        return res.status(404).json({ errors: [{ msg: 'Receipt not found' }] });
      }
      return res.status(200).json(receipt);
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };

  private deleteReceipt = async (req: RequestWithUser, res: Response) => {
    try {
      const receipt = await this.Receipt.findById(req.params._id);
      if (!receipt) {
        return res.status(404).json({ errors: [{ msg: 'Receipt not found' }] });
      }
      await receipt.remove();
      return res.status(200).json({ msg: 'Receipt removed' });
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };

  private getSlipImage = async (req: Request, res: Response) => {
    try {
      const { filename } = req.params;
      return res
        .status(200)
        .sendFile(
          path.join(__dirname, '..', 'storages', 'slip_images', filename)
        );
    } catch (e) {
      Logger.info(e.message);
      return res.status(500).json({ errors: [{ msg: 'Server error' }] });
    }
  };
}
