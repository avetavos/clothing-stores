export default interface IReceipt {
  user: String;
  list: IList[];
  receipt: string;
  slip: string;
}

interface IList {
  productId: string;
  description: string;
  quantity: number;
  unitPrice: number;
  amount: number;
}
