export default interface IProduct {
  name: String;
  barcode: String;
  price: Number;
  cost: Number;
  quantity: Number;
  date: Date;
}
