export default interface IUser {
  name: string;
  username: string;
  password?: string;
  phone: string;
  facebook: string;
  line: string;
  role: string;
  address: string;
}
