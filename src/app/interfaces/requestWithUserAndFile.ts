import { Request } from 'express';

export default interface IRequestWithUserAndFile extends Request {
  user: {
    _id: string;
    name: string;
  };
  files?: any;
}
